import React, { Component } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

/* Terceira tela */
export default class AdeusScreen extends Component {
  /* Título da Tela */
  static navigationOptions = {
    title: 'Se cuide!',
    headerStyle: {
      backgroundColor: 'red',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
  render() {
    const nome = this.props.navigation.getParam('nome');
    const email = this.props.navigation.getParam('email');
    return (
      <View style={styles.container}>
        <View style={styles.containerTitulo}>
          <Text style={styles.titulo}>
            Adeus {nome}.{'\n'}Continue se cuidando!!!!
          </Text>
          <Text style={styles.titulo}>
            Enviei uma mensagem com instruções para:{'\n'}
            {email}!!!!
          </Text>
        </View>

        <View style={styles.containerBotao}>
          <Button
            title="Ir para a Tela Inicial"
            onPress={() => this.props.navigation.navigate('Home')}
          />
        </View>

        <View style={styles.containerBotao}>
          <Button
            title="Voltar"
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  containerTitulo: {
    alignItems: 'center',
  },
  containerBotao: {
    margin: 20,
  },
  titulo: {
    textAlign: 'center',
    fontSize: 30,
    padding: 20,
  },
});
