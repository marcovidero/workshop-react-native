import React, { Component } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

/* Segunda tela */
export default class OlaScreen extends Component {
  /* Título da Tela */
  static navigationOptions = {
    title: 'Olá, tudo bem?',
    headerStyle: {
      backgroundColor: 'green',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  render() {
    /* A variável nome carrega o parâmetro passado */
    const nome = this.props.navigation.getParam('nome');
    const email = this.props.navigation.getParam('email');

    return (
      <View style={styles.container}>
        <View style={styles.containerTitulo}>
          <Text style={styles.titulo}>Olá {nome}, tudo bem?</Text>
        </View>

        <View style={styles.containerBotao}>
          <Button
            title="Ir para a Tela de Adeus"
            onPress={() =>
              this.props.navigation.navigate('Adeus', {
                nome: nome,
                email: email,
              })
            }
          />
        </View>

        <View style={styles.containerBotao}>
          <Button
            title="Voltar"
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  containerTitulo: {
    alignItems: 'center',
  },
  containerBotao: {
    margin: 20,
  },
  titulo: {
    textAlign: 'center',
    fontSize: 30,
  },
});
