import React, { Component } from 'react';
import { Button, StyleSheet, Text, TextInput, View } from 'react-native';

/* Cria Tela Principal */
/* Interface */
export default class HomeScreen extends Component {
  static navigationOptions = {
    title: 'Home',
    headerStyle: {
      backgroundColor: 'blue',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  constructor(props) {
    super(props);
    /* define os campos que quero preencher na tela */
    this.state = {
      nome: '',
      email: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerTitulo}>
          <Text style={styles.titulo}>Informe seus dados:</Text>
        </View>

        <View style={styles.containerConteudo}>
          <Text style={styles.texto}>Nome:</Text>
          <TextInput
            style={styles.entrada}
            placeholder="nome"
            onChangeText={(nome) => this.setState({ nome })}
          />
        </View>

        <View style={styles.containerConteudo}>
          <Text style={styles.texto}>Email:</Text>
          <TextInput
            style={styles.entrada}
            placeholder="email"
            onChangeText={(email) => this.setState({ email })}
          />
        </View>

        <View style={styles.containerBotao}>
          <Button
            title="Ir para a Tela de olá"
            onPress={() =>
              this.props.navigation.navigate('Ola', {
                nome: this.state.nome,
                email: this.state.email,
              })
            }
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  containerTitulo: {
    alignItems: 'center',
  },
  containerConteudo: {
    alignItems: 'center',
    padding: 20,
  },
  containerBotao: {
    margin: 20,
  },
  titulo: {
    textAlign: 'center',
    fontSize: 30,
  },
  texto: {
    fontSize: 20,
    color: 'blue',
  },
  entrada: {
    height: 40,
    fontSize: 20,
    padding: 10,
  },
});
