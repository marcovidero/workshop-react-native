import React, { Component } from 'react';

/* Imports necessários */

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import { HomeScreen, AdeusScreen, OlaScreen } from './screens';

/* Rota são as  Telas */
/* recebe dois parâmetro. O primeiro são as telas que teremos na aplicação e o segundo parâmetro é a Tela inicial (rota inicial) */
const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Ola: {
      screen: OlaScreen,
    },
    Adeus: {
      screen: AdeusScreen,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default function App() {
  return <AppContainer />;
}