---
title: "Ambiente WEB"
---
# Ambiente WEB

Antigamente, se quiséssemos testar uma tecnologia, tinhamos que preparar todo um ambiente local, um processo muitas vezes demorado e complexo. Hoje é possível testar novas tecnologias em plataformas WEB, como [Codepen](https://codepen.io/) e [JSFiddle](https://jsfiddle.net/), sem a necessidade de instalar nada no seu micro.

Para _React Native_, utilizaremos o Snack, uma ferramenta da plataforma _Expo_.

_Expo_ é um _framework_ e uma plataforma universal para aplicações _React_.
Ele é um conjunto de ferramentas e serviços construídos em torno do _React Native_ e plataformas nativas que ajudam a criar, construir, entregar e iterar rapidamente em sistemas móveis baseados em iOS ou Android e aplicações web a partir do mesmo código _JavaScript/TypeScript_.

Utilizaremos o Expo para apresentação do código e realização dos exercícios.

## Criação de conta no Expo

!!! warning "Atenção"
    
    A criação de uma conta é opcional, ou seja, não é uma condição necessária para a realização dos exercícios, entretanto, será útil caso desejem se aprofundar no desenvolvimento com _React Native_ ou salvar o andamento dos exercícios.

    Caso não queira criar a conta, avance para etapa seguinte.

1) Acesse o site do [Expo.dev](https://expo.dev/).

![](../assets/expo01.png)

2) Clique no botão ![](../assets/expo02.png).

3) Preencha os campos a seguir:

![](../assets/expo03.png)

4) Clique no botão ![](../assets/expo04.png).

5) Na tela que se abrirá, pode clicar em ++"Skip"++.

![](../assets/expo05.png)

6) Aparecerá a tela principal do Expo.

![](../assets/expo06.png)

## Conhecendo o _Snack_

1) Clique no botão ![](../assets/expo09.png) no menu do _Expo_.

![](../assets/expo08.png)

Aparecerá a seguinte página:

![](../assets/expo10.png).

2) Crie um novo _Snack_ clicando no botão ![](../assets/expo11.png). Aparecerá a seguinte tela.

![](../assets/expo13.png)

!!! info "Legenda"

    1. Arquivos do projeto
    1. Conteúdo do arquivo aberto
    1. Visualização em tempo real do aplicativo
    1. Logs e mensagens de erro