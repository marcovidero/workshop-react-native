---
title: "Ambiente local"
---
# Ambiente local

Para realização dos exercícios será necessário preparar o ambiente.

Orientaremos como instalar e configurar cada um dos requisitos:

1. Git
1. Node.js
1. Visual Studio Code
1. Expo (Go e CLI)
1. Componentes p/ apresentação

!!! attention "Atenção"

    Todos os procedimentos mostrados a seguir foram realizados em sistema operacional Windows 10.

    Para instalar os requisitos em outro sistema operacional, será necessário buscar orientações em seus respectivos sites.

!!! note "Nota"
    
    Para facilitar a instalação dos requisitos, utilizaremos o Chocolatey, um gerenciador de pacotes de instalação para Windows.

## Instalação do Chocolatey

Para instalar o Chocolatey será necessário abrir um _Prompt de Comando_ ou _PowerShell_ **com privilégios de administrador**. Mostraremos a seguir como abrir o primeiro.

1) Pesquise por `prompt`na barra de pesquisa do _Windows_.

![](../assets/cmd01.png)

2) Clique em executar como administrador.

3) Será solicitada elevação de privilégio. Clique no botão ++"Sim"++ ou forneça a senha de administrador para permitir a instalação.

4) Execute o comando a seguir.

=== "Prompt de Comando"

    ``` batch
    @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
    ```

=== "PowerShell"

    ``` powershell
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    ```

??? example "Exemplo"
    ``` hl_lines="4-9 23-24 44"
    Microsoft Windows [versão 10.0.19042.1110]
    (c) Microsoft Corporation. Todos os direitos reservados.

    C:\Windows\system32>@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe"
      -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command
        "[System.Net.ServicePointManager]::SecurityProtocol = 3072;
      iex ((New-Object System.Net.WebClient).DownloadString
        ('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;
      %ALLUSERSPROFILE%\chocolatey\bin"
    Forcing web requests to allow TLS v1.2 (Required for requests to Chocolatey.org)
    Getting latest version of the Chocolatey package for download.
    Not using proxy.
    Getting Chocolatey from
      https://community.chocolatey.org/api/v2/package/chocolatey/0.10.15.
    Downloading https://community.chocolatey.org/api/v2/package/chocolatey/0.10.15
      to C:\Users\ADMINI~1\AppData\Local\Temp\chocolatey\chocoInstall\chocolatey.zip
    Not using proxy.
    Extracting C:\Users\ADMINI~1\AppData\Local\Temp\chocolatey\chocoInstall\chocolatey.zip
      to C:\Users\ADMINI~1\AppData\Local\Temp\chocolatey\chocoInstall
    Installing Chocolatey on the local machine
    Creating ChocolateyInstall as an environment variable (targeting 'Machine')
      Setting ChocolateyInstall to 'C:\ProgramData\chocolatey'
    WARNING:
      It's very likely you will need to close and reopen your shell before you can use choco.
    Restricting write permissions to Administrators
    We are setting up the Chocolatey package repository.
    The packages themselves go to 'C:\ProgramData\chocolatey\lib'
      (i.e. C:\ProgramData\chocolatey\lib\yourPackageName).
    A shim file for the command line goes to 'C:\ProgramData\chocolatey\bin'
      and points to an executable in 'C:\ProgramData\chocolatey\lib\yourPackageName'.
    Creating Chocolatey folders if they do not already exist.
    WARNING: You can safely ignore errors related to missing log files when
      upgrading from a version of Chocolatey less than 0.9.9.
      'Batch file could not be found' is also safe to ignore.
      'The system cannot find the file specified' - also safe.
    chocolatey.nupkg file not installed in lib.
    Attempting to locate it from bootstrapper.
    PATH environment variable does not have C:\ProgramData\chocolatey\bin in it. Adding...
    AVISO: Not setting tab completion: Profile file does not exist at
    'C:\Users\Administrador\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1'.
    Chocolatey (choco.exe) is now ready.
    You can call choco from anywhere, command line or powershell by typing choco.
    Run choco /? for a list of functions.
    You may need to shut down and restart powershell and/or consoles first prior to using choco.
    Ensuring Chocolatey commands are on the path
    Ensuring chocolatey.nupkg is in the lib folder

    C:\Windows\system32>
    ```

5) Feche o _Prompt de comando_.

## Instalação do _Git_, _Node.js_ e _Visual Studio Code_

Para instalar os requisitos, siga os seguintes passos:

!!! warning "Atenção"

    Se você já possui os requisitos instalados na máquina, avance para o [próximo tópico](../c_expo-cli/).

1) Abra um _Prompt de Comando_ **com privilégio de administrador**.

2) Verifique se você já tem algum dos requisitos instalados.

```
choco list --local-only --include-programs
```

??? example "Exemplo"

    ``` hl_lines="1 6 8-9 11 18"
    C:\Windows\system32>choco list --local-only --include-programs
    Chocolatey v0.10.15
    chocolatey 0.10.15
    chocolatey-core.extension 1.3.5.1
    DotNet4.5.2 4.5.2.20140902
    git 2.32.0.2
    git.install 2.32.0.2
    nodejs-lts 14.17.3
    vscode 1.58.2
    vscode.install 1.58.2
    8 packages installed.

    7-Zip 19.00 (x64)|19.00
    Adobe Acrobat Reader 2020 MUI|20.004.30006

         <-- Conteúdo suprimido -->
    
    40 applications not managed with Chocolatey.

    C:\Windows\system32>
    ```

3) Execute o seguinte comando.

``` batch
choco install --yes git nodejs-lts vscode
```

!!! warning "Exclua do comando acima os aplicativos que já estiverem instalados"

    Pois caso algum deles já esteja instalado, aparecerá um erro como esse:
    
    ``` hl_lines="1 3"
    git v2.32.0.2 already installed.
    Use --force to reinstall, specify a version to install, or try upgrade.
    Chocolatey installed 0/1 packages.
    See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log).
    Warnings:
    - git - git v2.32.0.2 already installed.
    Use --force to reinstall, specify a version to install, or try upgrade.
    ```

??? example "Exemplo"

    ``` hl_lines="4 6-7 33 46 79 86-93"
    Microsoft Windows [versão 10.0.19042.1110]
    (c) Microsoft Corporation. Todos os direitos reservados.

    C:\Windows\system32>choco install --yes git nodejs-lts vscode
    Chocolatey v0.10.15
    Installing the following packages:
    git;nodejs-lts;vscode
    By installing you accept licenses for the packages.
    Progress: Downloading git.install 2.32.0.2... 100%
    Progress: Downloading chocolatey-core.extension 1.3.5.1... 100%
    Progress: Downloading git 2.32.0.2... 100%

    chocolatey-core.extension v1.3.5.1 [Approved]
    chocolatey-core.extension package files install completed. Performing other installation steps.
    Installed/updated chocolatey-core extensions.
    The install of chocolatey-core.extension was successful.
      Software installed to 'C:\ProgramData\chocolatey\extensions\chocolatey-core'

    git.install v2.32.0.2 [Approved]
    git.install package files install completed. Performing other installation steps.
    Using Git LFS
    Installing 64-bit git.install...
    git.install has been installed.
    git.install installed to 'C:\Program Files\Git'
      git.install can be automatically uninstalled.
    Environment Vars (like PATH) have changed. Close/reopen your shell to
    see the changes (or in powershell/cmd.exe just type `refreshenv`).
    The install of git.install was successful.
      Software installed to 'C:\Program Files\Git\'

    git v2.32.0.2 [Approved]
    git package files install completed. Performing other installation steps.
    The install of git was successful.
      Software install location not explicitly set, could be in package or
      default install location if installer.
    Progress: Downloading nodejs-lts 14.17.3... 100%

    nodejs-lts v14.17.3 [Approved]
    nodejs-lts package files install completed. Performing other installation steps.
    Installing 64 bit version
    Installing nodejs-lts...
    nodejs-lts has been installed.
      nodejs-lts may be able to be automatically uninstalled.
    Environment Vars (like PATH) have changed. Close/reopen your shell to
    see the changes (or in powershell/cmd.exe just type `refreshenv`).
    The install of nodejs-lts was successful.
      Software installed as 'MSI', install location is likely default.
    Progress: Downloading vscode.install 1.58.2... 100%
    Progress: Downloading DotNet4.5.2 4.5.2.20140902... 100%
    Progress: Downloading vscode 1.58.2... 100%

    DotNet4.5.2 v4.5.2.20140902 [Approved]
    dotnet4.5.2 package files install completed. Performing other installation steps.
    Microsoft .Net 4.5.2 Framework is already installed on your machine.
    The install of dotnet4.5.2 was successful.
      Software install location not explicitly set, could be in package or
      default install location if installer.

    vscode.install v1.58.2 [Approved]
    vscode.install package files install completed. Performing other installation steps.
    Merge Tasks: !runCode, desktopicon, quicklaunchicon, addcontextmenufiles,
      addcontextmenufolders, associatewithfiles, addtopath
    Downloading vscode.install 64 bit
      from 'https://az764295.vo.msecnd.net/stable/c3f126316369cd610563c75b1b1725e0679adfb3/VSCodeSetup-x64-1.58.2.exe'
    Progress: 100% - Completed download of
      C:\Users\Administrador\AppData\Local\Temp\chocolatey\vscode.install\1.58.2\VSCodeSetup-x64-1.58.2.exe (76.8 MB).
    Download of VSCodeSetup-x64-1.58.2.exe (76.8 MB) completed.
    Hashes match.
    Installing vscode.install...
    vscode.install has been installed.
      vscode.install can be automatically uninstalled.
    Environment Vars (like PATH) have changed. Close/reopen your shell to
    see the changes (or in powershell/cmd.exe just type `refreshenv`).
    The install of vscode.install was successful.
      Software installed to 'C:\Program Files\Microsoft VS Code\'

    vscode v1.58.2 [Approved]
    vscode package files install completed. Performing other installation steps.
    The install of vscode was successful.
      Software install location not explicitly set, could be in package or
      default install location if installer.

    Chocolatey installed 7/7 packages.
    See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log).

    Installed:
    - chocolatey-core.extension v1.3.5.1
    - nodejs-lts v14.17.3
    - vscode.install v1.58.2
    - git.install v2.32.0.2
    - vscode v1.58.2
    - dotnet4.5.2 v4.5.2.20140902
    - git v2.32.0.2

    C:\Windows\system32>
    ```

4) Feche o _Prompt de comando_.

!!! info "Dica"
    
    Para desinstalar algum pacote previamente instalado pelo Chocolatey, use o comando `choco uninstall` seguido do nome do pacote.
    
    Exemplo:
    ```
    choco uninstall --yes nodejs-lts
    ```

## Instalação do Expo

O Expo é um _framework_ e uma plataforma universal para aplicações _React_.
Ele é um conjunto de ferramentas e serviços construídos em torno do _React Native_ e plataformas nativas que ajudam a criar, construir, entregar e iterar rapidamente em sistemas móveis baseados em iOS ou Android e aplicações web a partir do mesmo código _JavaScript/TypeScript_.

Utilizaremos o Expo para apresentação do código e realização dos exercícios.

### Instalação do _Expo Go_

O _Expo Go_ é um aplicativo que possibilita testar o seu aplicativo no smartphone antes de publicar na Apple Store ou  Play Store.

1) Clique num dos botões para instalar o aplicativo no seu smartphone.

[:material-apple: iOS App](https://itunes.apple.com/app/apple-store/id982107779){ .md-button } [:material-android: Android App](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www){ .md-button }

### Instalação do _Expo-cli_

Para instalar o _Expo-cli_ no computador, usaremos o _NPM_ do _Node.js_.

1) Abra um _Prompt de Comando_.

![](../assets/cmd02.png)

2) Digite o seguinte comando.

```
npm install --global expo-cli
```

??? example "Exemplo"

    ``` hl_lines="4 52-53"
    Microsoft Windows [versão 10.0.19042.1110]
    (c) Microsoft Corporation. Todos os direitos reservados.

    C:\Users\Marco>npm install --global expo-cli
    npm WARN deprecated uuid@3.4.0: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
    npm WARN deprecated core-js@2.6.12: core-js@<3.3 is no longer maintained and not recommended for usage due to the number of issues. Because of the V8 engine whims, feature detection in old core-js versions could cause a slowdown up to 100x even if nothing is polyfilled. Please, upgrade your dependencies to the actual version of core-js.
    npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
    npm WARN deprecated har-validator@5.1.5: this library is no longer supported
    npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
    npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
    npm WARN deprecated uuid@3.3.2: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
    npm WARN deprecated querystring@0.2.1: The querystring API is considered Legacy. new code should use the URLSearchParams API instead.
    npm WARN deprecated querystring@0.2.0: The querystring API is considered Legacy. new code should use the URLSearchParams API instead.
    npm WARN deprecated chokidar@2.1.8: Chokidar 2 will break on node v14+. Upgrade to chokidar 3 with 15x less dependencies.
    npm WARN deprecated fsevents@1.2.13: fsevents 1 will break on node v14+ and could be using insecure binaries. Upgrade to fsevents 2.
    npm WARN deprecated @hapi/joi@16.1.8: Switch to 'npm install joi'
    npm WARN deprecated @hapi/hoek@8.5.1: This version has been deprecated and is no longer supported or maintained
    npm WARN deprecated @hapi/topo@3.1.6: This version has been deprecated and is no longer supported or maintained
    npm WARN deprecated @hapi/address@2.1.4: Moved to 'npm install @sideway/address'
    npm WARN deprecated @hapi/formula@1.2.0: Moved to 'npm install @sideway/formula'
    npm WARN deprecated @hapi/pinpoint@1.0.2: Moved to 'npm install @sideway/pinpoint'
    npm WARN deprecated uuid@3.0.0: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
    npm WARN deprecated uuid@2.0.3: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
    C:\Users\Marco\AppData\Roaming\npm\expo -> C:\Users\Marco\AppData\Roaming\npm\node_modules\expo-cli\bin\expo.js
    C:\Users\Marco\AppData\Roaming\npm\expo-cli -> C:\Users\Marco\AppData\Roaming\npm\node_modules\expo-cli\bin\expo.js

    > core-js@2.6.12 postinstall C:\Users\Marco\AppData\Roaming\npm\node_modules\expo-cli\node_modules\core-js
    > node -e "try{require('./postinstall')}catch(e){}"

    Thank you for using core-js ( https://github.com/zloirock/core-js ) for polyfilling Java standard library!

    The project needs your help! Please consider supporting of core-js on Open Collective or Patreon:
    > https://opencollective.com/core-js
    > https://www.patreon.com/zloirock

    Also, the author of core-js ( https://github.com/zloirock ) is looking for a good job -)


    > ejs@2.7.4 postinstall C:\Users\Marco\AppData\Roaming\npm\node_modules\expo-cli\node_modules\ejs
    > node ./postinstall.js

    Thank you for installing EJS: built with the Jake Java build tool (https://jakejs.com/)

    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@~2.3.2 (node_modules\expo-cli\node_modules\chokidar\node_modules\fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.3.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules\expo-cli\node_modules\watchpack-chokidar2\node_modules\chokidar\node_modules\fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules\expo-cli\node_modules\webpack-dev-server\node_modules\chokidar\node_modules\fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
    npm WARN @pmmmwh/react-refresh-webpack-plugin@0.3.3 requires a peer of react-refresh@^0.8.2 but none is installed. You must install peer dependencies yourself.

    + expo-cli@4.8.1
    added 1807 packages from 786 contributors in 158.36s

    C:\Users\Marco>
    ```

3) Feche o _Prompt de comando_.

## Teste do ambiente

Para testar o ambiente, criaremos uma pequena aplicação de exemplo.

1) Abra um _Prompt de comando_.

![](../assets/cmd02.png)

2) Crie uma pasta para armazenar os projetos.

```
mkdir C:\react-native
```

3) Mude para pasta criada no passo anterior.

```
cd C:\react-native
```

4) Crie um projeto com a ajuda do _Expo-cli_.

```
expo init ola
```

5) Escolha a opção `blank`.

```doscon hl_lines="4"
C:\react-native>expo init ola
? Choose a template: » - Use arrow-keys. Return to submit.
    ----- Managed workflow -----
>   blank               a minimal app as clean as an empty canvas
    blank (TypeScript)  same as blank but with TypeScript configuration
    tabs (TypeScript)   several example screens and tabs using react-navigation and TypeScript
    ----- Bare workflow -----
    minimal             bare and minimal, just the essentials to get you started
```

6) Entre na pasta que acabou de criar.

```
cd ola 
```

7) Inicie o Expo.

```
expo start
```

???+ example "Exemplo"

    ``` hl_lines="1"
    C:\Users\Marco\Meu Drive\Mobile\ReactNative\HelloWorld>expo start
    Starting project at C:\Users\Marco\Meu Drive\Mobile\ReactNative\HelloWorld
    Developer tools running on http://localhost:19002
    Starting Metro Bundler
    ```
    ![](../assets/expo-cli01.png)
    ``` linenums="5"
    › Waiting on exp://192.168.68.117:19000
    › Scan the QR code above with Expo Go (Android) or the Camera app (iOS)

    › Press a │ open Android
    › Press w │ open web

    › Press r │ reload app
    › Press m │ toggle menu
    › Press d │ show developer tools
    › shift+d │ toggle auto opening developer tools on startup (disabled)

    › Press ? │ show all commands

    Logs for your project will appear below. Press Ctrl+C to exit.
    ```

8) Para acessar o aplicativo, escolha uma das opções abaixo e siga as instruções.

=== "Smartphone"
    ![](../assets/expogo.png) Abra o app _Expo Go_ no seu smartphone e escaneie o QRCode.

=== "Web"
    Tecle ++w++ para abrir no navegador padrão.
    
    !!! warning "Atenção"

        O Chrome costuma alterar o endereço de HTTP para HTTPS, o que acaba inviabilizando seu uso.
        
        Nesse caso, utilize o Firefox.

9) Abra o arquivo _App.js_ no VSCode, substitua o conteúdo da tag `<Text>` por "Olá, tudo bem?" e salve.

???+ example "Exemplo"
    ```
    import { StatusBar } from 'expo-status-bar';
    import React from 'react';
    import { StyleSheet, Text, View } from 'react-native';

    export default function App() {
    return (
        <View style={styles.container}>
        {==<Text>==}{--Open up App.js to start working on your app!--}{++Olá, tudo bem?++}{==</Text>==}
        <StatusBar style="auto" />
        </View>
    );
    }

    const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    });
    ```

A tela atualizará automaticamente.

![](../assets/helloworld01.png) ![](../assets/helloworld02.png)

## Componentes p/ apresentação

Necessitaremos de alguns componentes no workshop.

Para evitar contratempos no dia da apresentação, sugerimos que instalem antecipadamente esses requisitos.

1) No _Prompt de comando_ digite os comandos a seguir.

``` doscon
C:\react-native\ola\> npm install --save react-navigation
C:\react-native\ola\> npm install --save react-navigation-stack
C:\react-native\ola\> npm install --save react-native-gesture-handler
C:\react-native\ola\> npm install -g react-native-cli
C:\react-native\ola\> npm install
C:\react-native\ola\> react-native link  react-native-gesture-handler
```

*[CLI]: Command-line Interface
*[VSCode]: Visual Studio Code