---
title: "Escolha do ambiente"
---
# Preparação do ambiente

Para acompanhamento do workshop e realização dos exercícios será necessário preparar um ambiente de desenvolvimento em _React Native_.

Utilizaremos as seguintes tecnologias:

- Expo
- Git
- Node.js
- React
- Visual Studio Code

Demonstraremos nas próximas páginas o processo de instalação e configuração de cada uma delas em ambiente local ou WEB.