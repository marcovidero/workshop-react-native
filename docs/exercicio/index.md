---
title: "Intruções"
---
# Intruções

## Código inicial de exemplo

Para realizar o exercício, será necessário baixar o código fonte disponível em [https://gitlab.com/marcovidero/workshop-react-native/-/tree/main/src](https://gitlab.com/marcovidero/workshop-react-native/-/tree/main/src).

```
src
├── screens
│   ├── AdeusScreen.js
│   ├── HomeScreen.js
│   ├── index.js
│   └── OlaScreen.js
└── App.js 
```

## Exercício

A partir do código fornecido como exemplo:

1) Inclua o campo _"fone"_ na tela inicial do aplicativo.

=== "Antes"
    ![](../assets/expogo01.png)

=== "Depois"
    ![](../assets/expogo01depois.png)

!!! tip "Dica"
    A aparência da segunda tela não irá mudar, mas ela deverá conter as informações do campo fone definidas na tela anterior.

    ![](../assets/expogo02.png)

2) Mostre o _"fone"_ na última tela.

=== "Antes"
    ![](../assets/expogo03.png)

=== "Depois"
    ![](../assets/expogo03depois.png)

