---
title: "Roteiro de testes"
---

# Roteiro de testes

- Inclusão do campo _"Fone:"_ na tela inicial.
    - [ ] Definição da variável _"fone"_.
    - [ ] Inclusão do campo na tela.
    - [ ] Armazenamento da informação digitada.
- Inclusão, na última tela, do _"Fone"_ informado na tela inicial.
    - [ ] Recuperação da variável _"fone"_.
    - [ ] Inclusão do campo na tela.
