[![pipeline status](https://gitlab.com/marcovidero/workshop-react-native/badges/main/pipeline.svg)](https://gitlab.com/marcovidero/workshop-react-native/-/commits/main)

# Disciplina: Desenvolvimento para dispositivos móveis - 2021

## Workshop sobre desenvolvimento para dispositivos móveis

### React Native

Site criado para divulgação do material a ser apresentado no Workshop sobre React Native, na disciplina Desenvolvimento para dispositivos móveis, da pós-graduação UFBA - TRE/BA, em 2021.

#### Equipe

- Gabriel Chaves
- Isabela Plessim
- Lucas Andrade
- Marco Vídero
- Mirela Casado